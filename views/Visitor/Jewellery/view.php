<?php
require_once("../../../vendor/autoload.php");

$objJewellery = new \App\Jewellery\Jewellery();
$objJewellery->setData($_GET);
$oneData = $objJewellery->view();


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jewellery - Single Jewellery Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>


        td{
            border: 0px;
        }

        table{
            border: 1px;
            font-style: italic;
        }

        tr{
            height: 30px;
        }

        body{
            background-image: url("b.jpg");
        }
    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center" ;">Jewellery - Single Jewellery Information</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Name</th>
            <th>Jewellery Type</th>
            <th>Jewellery Picture</th>
            <th>Action Buttons</th>
        </tr>

        <?php

            echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>

                     <td>$oneData->jewellery_pic</td>
                     <td style='padding-left: 3%'><img src='UploadedFiles/$oneData->jewellery_pic' style=\"width:300px;height:300px;\" /></td>


                     <td><a href='index.php' class='btn btn-info'>Back To Active List</a> </td>
                  </tr>
              ";

        ?>

    </table>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>