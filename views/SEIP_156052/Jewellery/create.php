<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div id='message'> $msg </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Luxury jewellery & accessories Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <style>


        td{
            border: 0px;
        }

        table{
            border: 10px;
            font-style: italic;
        }

        tr{
            height: 30px;
        }

        body{
            background-image: url("b.jpg");
        }
    </style>













</head>
<body>


<div class="container">

    <div class="navbar">

        <td><a href='index.php' class='btn btn-group-lg btn-info'>Available-Jewellery List</a> </td>

    </div>



<form  class="form-group f" action = "store.php" method = "post" enctype="multipart/form-data">
    Please Enter Jewellery's Name:
    <br>
    <input class="form-control" type="text" name="name">
    <br>
    Enter Jewellery's  Picture:
    <input type = "file" name="picture" accept=".png, .jpg, .jpeg" >
    <br>
    <input type="submit">
    <br>

</form>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>


</body>
</html>