-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2017 at 07:07 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicproject_skutz_b44`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(20) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `soft_deleted` varchar(123) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`, `soft_deleted`) VALUES
(1, 'nousheen', '1992-08-07', 'NO'),
(2, 'suraia', '1993-02-13', 'NO'),
(3, 'Tasnin', '1995-07-03', 'NO'),
(4, 'kanij', '1996-04-09', 'No'),
(5, 'Sadia', '1998-02-13', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Pyramid', 'Tom Martin', 'No'),
(2, 'Inferno', 'Dan Brown', 'No'),
(3, 'Himu', 'Humayun Ahmed', 'Yes'),
(4, 'vcchgg', 'fdsfd', 'No'),
(5, 'gdfgd', '547646', 'Yes'),
(6, 'ertdy', '346477', 'No'),
(7, 'fdsd fs', 'sfd sf df', 'No'),
(8, 'Durbin', 'Shirshendu', 'Yes'),
(9, 'sfds', 'sdfertwt3', 'No'),
(10, 'dfs', 'fds sar', 'No'),
(11, 'sdfsf', 'sdfsg', 'No'),
(12, 'Mr Y', 'Mr X', 'No'),
(13, 'fdgdfg', 'dsfgdsdyr', 'No'),
(14, 'stest', 'fdsgdsg', 'No'),
(15, 'sdfsfs', 'sfdsf', 'No'),
(16, 'sdgfs', 'sdgf', 'No'),
(17, 'dfs fd', 'sdf sgt ert', 'No'),
(18, 'dfs fd', 'sdf sgt ert', 'No'),
(19, 'dsfs f', 'dy rt ryry', 'No'),
(20, 'dsfs f', 'dy rt ryry', 'No'),
(21, 'dfsf', 'sdfsf', 'No'),
(22, 'Amar Golpo', 'Mahbubur Rahman', 'YES');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(14) NOT NULL,
  `city` varchar(134) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(22) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `soft_deleted`) VALUES
(1, 'Dhaka', 'No'),
(2, 'Phnom Penh', 'No'),
(3, 'Siem Reap', 'No'),
(4, 'Dhaka', 'NO'),
(5, 'Siem Reap', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_deleted`) VALUES
(1, 'suriyayeasmin', 'suriya123@gmail.com', 'No'),
(2, 'nowshin chowdhury', 'nowshin123@gmail.com', 'No'),
(3, 'vvv', 'lubu@gmail.com', 'no'),
(4, 'hhgh', 'suru@hmail.com', 'yes'),
(5, 'jahan', 'jahan123@yahoo.com', 'yes'),
(6, 'Tasnin', 't@gmail.com', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(20) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobby`, `soft_deleted`) VALUES
(1, 'nousheen', 'Swimming,Writing', 'NO'),
(2, 'Mousumi', 'Gardening,Drawing', 'NO'),
(3, 'Sadia', 'Swimming', 'NO'),
(4, 'suraia', 'Reading,Gardening,Drawing', 'NO'),
(5, 'Tasnin', 'Gardening,Drawing', 'NO'),
(6, 'Munni', 'Swimming', 'NO'),
(7, 'Trisha', 'Writing,Reading', 'NO'),
(8, 'kanij', 'Gardening,Drawing', 'NO'),
(9, 'jdsdcsdkb', 'Gardening', 'NO'),
(10, 'neel', 'Drawing', 'Yes'),
(11, 'nishu', 'Gardening', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_pic`, `soft_deleted`) VALUES
(31, 'p1', 'propic1.jpg', 'No'),
(32, 'p2', 'cute (5).jpg', 'No'),
(33, 'p3', 'propic1.jpg', 'No'),
(34, 'p4', 'propic2.jpg', 'No'),
(35, 'p5', 'propic5.jpg', 'Yes'),
(36, 'p7', 'propic3.jpg', 'Yes'),
(37, 'mitu', 'maxresdefault.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `students_genders`
--

CREATE TABLE `students_genders` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students_genders`
--

INSERT INTO `students_genders` (`id`, `name`, `gender`, `soft_deleted`) VALUES
(1, 'Sadia', 'Female', 'No'),
(2, 'Nadia', 'Female', 'No'),
(3, 'Nowrin', 'Female', 'No'),
(4, 'Noah', 'Male', 'No'),
(5, 'Tasmiah', 'Female', 'No'),
(6, 'Arik', 'Male', 'No'),
(8, 'Student2', 'Male', 'Yes'),
(9, 'Student3', 'Female', 'Yes'),
(13, 'Miss X', 'Female', 'Yes'),
(14, 'Student3', 'Female', 'No'),
(15, 'sumaia', 'Female', 'No'),
(16, 'mitu', 'Female', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summaryoforganization`
--

CREATE TABLE `summaryoforganization` (
  `id` int(11) NOT NULL,
  `organization_name` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `summary` varchar(111) COLLATE utf16_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf16_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `summaryoforganization`
--

INSERT INTO `summaryoforganization` (`id`, `organization_name`, `summary`, `soft_deleted`) VALUES
(1, 'jhljkhjklbhjk', 'mm ,bm,,m ,m ,m mm,', 'Yes'),
(2, 'jhbjbkbjbjbkjk', 'jbjbjbjbjkhukguiguiguiggui', 'Yes'),
(3, 'tas', 'My organization ista s ', 'Yes'),
(4, 'xxsss', 'My organization is for ', 'Yes'),
(5, '', 'My organization is for ', 'No'),
(6, 'scd', 'My organization is for ', 'No'),
(8, 'aczdza', 'My organization is for ', 'No'),
(9, 'saasdsa', 'My organization is for ', 'No'),
(10, ',mml', 'My organization is for ', 'No'),
(11, 'kk,nn', 'My organization is forklnklnlnk ', 'No'),
(12, 'jhljkhjklbhjk', 'mnnk', 'No'),
(13, 'jhljkhjklbhjk', 'k/mkl', 'No'),
(14, 'khjhjhDJXK', 'My nklhilhiis for ', 'No'),
(15, 'jhljkhjklbhjk', 'mm ,bm,,m ,m ,m mm,', 'No'),
(16, 'jhljkhjklbhjk', 'mm ,bm,,m ,m ,m mm,', 'No'),
(17, 'jhljkhjklbhdjk', 'bxbbcfm ,bm,,m ,m ,m mm,', 'No'),
(18, 'jhljkhjklbhjk', 'mm ,bm,,m ,mjhkjhk ,m mm,', 'No'),
(19, 'Helllo', 'Helllo', 'No'),
(20, 'Helllo', 'Helllo', 'No'),
(21, 'jhljkhjklbhjk', 'hello', 'No'),
(22, 'hello', 'heelouiguiggui', 'No'),
(23, 'jhbjbkbjbjbkjk', 'go to hell', 'No'),
(24, 'vcvs', 'sdsxds', 'No'),
(25, 'hjfmg', 'mbnv', 'No'),
(26, '', '1', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_genders`
--
ALTER TABLE `students_genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `students_genders`
--
ALTER TABLE `students_genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
